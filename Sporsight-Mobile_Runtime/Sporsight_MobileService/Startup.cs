using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Sporsight_MobileService.Startup))]

namespace Sporsight_MobileService
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureMobileApp(app);
        }
    }
}