//
//  ClientManager.swift
//  Sporsight_Mobile
//
//  Created by Christopher Donoso on 7/25/18.
//  Copyright © 2018 Microsoft. All rights reserved.
//

import Foundation

class ClientManager {
    
    static let sharedClient = MSClient(applicationURLString: "https://sporsight-mobile.azurewebsites.net")
    
}
