import Foundation

public struct SporsightProducts {

  public static let SwiftShopping = "Premium"

  private static let productIdentifiers: Set<ProductIdentifier> = [SporsightProducts.SwiftShopping]

  public static let store = IAPHelper(productIds: SporsightProducts.productIdentifiers)
}

func resourceNameForProductIdentifier(_ productIdentifier: String) -> String? {
  return productIdentifier.components(separatedBy: ".").last
}
