//
//  CameraViewController.swift
//  SporSight
//
//  Created by Jack Mustacato on 3/7/18.
//  Copyright © 2018 Jack Mustacato and Christopher Donoso. All rights reserved.
//

import UIKit
import MobileCoreServices

class CameraViewController: UIViewController {

    @IBOutlet weak var connectionLabel: UILabel!
    
    let bluetoothRecordConnection = BluetoothConnection()
    
    let videoView = UIImagePickerController()
    public var isRecording = false {
        didSet {
            NSLog("%@", "Video recording: \(isRecording)")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bluetoothRecordConnection.delegate = self as? BluetoothConnectionDelegate
    }
    
    @IBAction func record(_ sender: Any) {
        
        bluetoothRecordConnection.send(videoSignal: "videoActivated")

        startVideo()
    }
    
    func startVideo() {
        videoView.sourceType = .camera
        videoView.mediaTypes = [kUTTypeMovie as String]
        videoView.allowsEditing = true
        videoView.delegate = self
        videoView.showsCameraControls = true
        
        let overlayView = UIView(frame: self.view.frame)
        let modelName = UIDevice.modelName
        var startStopView: UIView
        
        // view that will go on top of red record button
        if(modelName == "iPhone XS" || modelName == "iPhone X" || modelName == "iPhone XR")
        {
            startStopView = UIView(frame: CGRect(x: self.view.frame.size.width/2 - 35, y: self.view.frame.size.height - 146, width: 70, height: 70))
        }
            
        else
        {
            startStopView = UIView(frame: CGRect(x: self.view.frame.size.width/2 - 35, y: self.view.frame.size.height - 83, width: 70, height: 70))
        }
        
        startStopView.layer.cornerRadius = 0.5 * startStopView.bounds.size.width
        startStopView.clipsToBounds = true
        startStopView.isUserInteractionEnabled = true
        startStopView.backgroundColor = .red
        startStopView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(startStopTapped)))
        overlayView.addSubview(startStopView)
        
        videoView.cameraOverlayView = overlayView
        videoView.cameraOverlayView?.isHidden = false
        present(videoView, animated: true, completion: nil)
    }
    
    @objc func startStopTapped() {
        if isRecording {
            videoView.stopVideoCapture()
            videoView.cameraOverlayView?.isHidden = true
            isRecording = false
            bluetoothRecordConnection.send(videoSignal: "recordingStopped")
        } else if videoView.startVideoCapture() {
            isRecording = true
            bluetoothRecordConnection.send(videoSignal: "recordingActivated")
        }
    }
    
    @objc func video(_ videoPath: String, didFinishSavingWithError error: Error?, contextInfo info: AnyObject) {
        let title = (error == nil) ? "Success" : "Error"
        let message = (error == nil) ? "Video was saved" : "Video failed to save"
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}

// MARK: - UIImagePickerControllerDelegate

extension CameraViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        dismiss(animated: true, completion: nil)
        
        guard let mediaType = info[UIImagePickerControllerMediaType] as? String,
            mediaType == (kUTTypeMovie as String),
            let url = info[UIImagePickerControllerMediaURL] as? URL,
            UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(url.path)
            else { return }
        
        // Handle a movie capture
        UISaveVideoAtPathToSavedPhotosAlbum(url.path, self, #selector(video(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
}

// MARK: - UINavigationControllerDelegate


extension CameraViewController: UINavigationControllerDelegate {
}

extension CameraViewController : BluetoothConnectionDelegate {
    
    func connectedDevicesChanged(manager: BluetoothConnection, connectedDevices: [String]) {
        OperationQueue.main.addOperation {
            self.connectionLabel.text = "Connections: \(connectedDevices)"
        }
    }
    
    func videoButtonClicked(manager : BluetoothConnection, videoString: String) {
        OperationQueue.main.addOperation {
            switch videoString {
            case "videoActivated":
                self.startVideo()
            case "recordingActivated":
                self.videoView.startVideoCapture()
                self.isRecording = true
            case "recordingStopped":
                self.videoView.stopVideoCapture()
                self.videoView.cameraOverlayView?.isHidden = true
                self.isRecording = false
            default:
                NSLog("%@", "Unknown value received: \(videoString)")
            }
        }
    }
    
}

public extension UIDevice {
    
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod Touch 5"
            case "iPod7,1":                                 return "iPod Touch 6"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone XR"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad6,11", "iPad6,12":                    return "iPad 5"
            case "iPad7,5", "iPad7,6":                      return "iPad 6"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch)"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }
        
        return mapToDevice(identifier: identifier)
    }()
    
}
