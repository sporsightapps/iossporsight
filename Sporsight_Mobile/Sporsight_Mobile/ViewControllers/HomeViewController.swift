//
//  HomeViewController.swift
//  Sporsight_Mobile
//
//  Created by Christopher Donoso on 6/23/18.
//  Copyright © 2018 Microsoft. All rights reserved.
//


import AppCenter
import AppCenterAnalytics
import AppCenterCrashes
import Foundation
import MSAL

class HomeViewController: UIViewController {
 
    let kTenantName = "sporsightiam.onmicrosoft.com" // Your tenant name
    let kClientID = "7b66c778-b9c6-4e73-acf6-d26a969978be" // Your client ID from the portal when you created your application
    let kSignupOrSigninPolicy = "b2c_1_susi" // Your signup and sign-in policy you created in the portal
    let kEditProfilePolicy = "b2c_1_edit_profile" // Your edit policy you created in the portal
    let kGraphURI = "https://sporsightiam.onmicrosoft.com/hello" // This is your backend API that you've configured to accept your app's tokens
    let kScopes: [String] = ["https://sporsightiam.onmicrosoft.com/hello/demo.read"] // This is a scope that you've configured your backend API to look for.
    
    //https://sporsightiam.onmicrosoft.com/hello/demo.read
    //https://sporsightiam.onmicrosoft.com/hello/user_impersonation
    
    // DO NOT CHANGE - This is the format of OIDC Token and Authorization endpoints for Azure AD B2C.
    let kEndpoint = "https://login.microsoftonline.com/tfp/%@/%@"
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        MSAnalytics.trackEvent("Entered Home Page", withProperties: ["Category" : "Home Page"])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the Navigation Bar
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the Navigation Bar
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func alertButton(_ sender: Any) {
        MSAnalytics.trackEvent("Alert Button Clicked", withProperties: ["Category" : "Home Page"])
    }
    
    @IBAction func highlightsButton(_ sender: Any) {
        MSAnalytics.trackEvent("Highlights Button Clicked", withProperties: ["Category" : "Home Page"])
    }
    
    @IBAction func statisticsButton(_ sender: Any) {
        MSAnalytics.trackEvent("Statistics Button Clicked", withProperties: ["Category" : "Home Page"])
    }
    
    @IBAction func newsfeedButton(_ sender: Any) {
        MSAnalytics.trackEvent("Newsfeed Button Clicked", withProperties: ["Category" : "Home Page"])
    }
    
    @IBAction func recordButton(_ sender: Any) {
        MSAnalytics.trackEvent("Record Button Clicked", withProperties: ["Category" : "Home Page"])
    }
    
    @IBAction func playVideoButton(_ sender: Any) {
        MSAnalytics.trackEvent("Play Video Button Clicked", withProperties: ["Category" : "Home Page"])
        
//        let playVideoViewController = self.storyboard?.instantiateViewController(withIdentifier: "PlayVideoViewController") as! PlayVideoViewController
//        self.navigationController?.pushViewController(playVideoViewController, animated: true)
    }
    
    
    @IBAction func editAccountButton(_ sender: Any) {
        
        /**
         The way B2C knows what actions to perform for the user of the app is through the use of `Authority URL`,
         a URL indicating a directory that MSAL can use to obtain tokens. In Azure B2C
         it is of the form `https://<instance/tfp/<tenant>/<policy>`, where `<instance>` is the
         directory host (e.g. https://login.microsoftonline.com), `<tenant>` is a
         identifier within the directory itself (e.g. a domain associated to the
         tenant, such as contoso.onmicrosoft.com), and `<policy>` is the policy you wish to
         use for the current user flow. The policy we are using here is the `kEditProfilePolicy`
         as the app is going to allow the user to edit their profile.
         
         Note that we don't store the accessToken that is returend by the acquireToken call this time.
         This is because this acquireToken() call is only to invoke a policy of "editing the profile" and is not
         meant to be used to call any additional APIs. When you call policies beyond your "sign in" policy
         you should not store the access token as it's of no use to you.
         */
        
        let kAuthority = String(format: kEndpoint, kTenantName, kEditProfilePolicy)
        
        do {
            
            /**
             
             Initialize a MSALPublicClientApplication with a given clientID and authority
             
             - clientId:     The clientID of your application, you should get this from the app portal.
             - authority:    A URL indicating a directory that MSAL can use to obtain tokens. In Azure B2C
             it is of the form `https://<instance/tfp/<tenant>/<policy>`, where `<instance>` is the
             directory host (e.g. https://login.microsoftonline.com), `<tenant>` is a
             identifier within the directory itself (e.g. a domain associated to the
             tenant, such as contoso.onmicrosoft.com), and `<policy>` is the policy you wish to
             use for the current user flow.
             - error:       The error that occurred creating the application object, if any, if you're
             not interested in the specific error pass in nil.
             
             */
            
            let application = try MSALPublicClientApplication.init(clientId: kClientID, authority: kAuthority)
            
            /**
             Acquire a token for a new user using interactive authentication
             
             - forScopes: Permissions you want included in the access token received
             in the result in the completionBlock. Not all scopes are
             gauranteed to be included in the access token returned.
             - completionBlock: The completion block that will be called when the authentication
             flow completes, or encounters an error.
             */
            
            let thisUser = try self.getUserByPolicy(withUsers: application.users(), forPolicy: kEditProfilePolicy)
            
            application.acquireToken(forScopes: kScopes, user:thisUser ) { (result, error) in
                if error == nil {
                    print("Successfully edited profile")
                    
                    
                } else {
                    print("Could not edit profile: \(error ?? "No error informarion" as! Error)")
                }
            }
        } catch {
            print("Unable to create application \(error)")
        }
    }
    
    
    @IBAction func signoutButton(_ sender: Any) {
        
        /**
         The way B2C knows what actions to perform for the user of the app is through the use of `Authority URL`,
         a URL indicating a directory that MSAL can use to obtain tokens. In Azure B2C
         it is of the form `https://<instance/tfp/<tenant>/<policy>`, where `<instance>` is the
         directory host (e.g. https://login.microsoftonline.com), `<tenant>` is a
         identifier within the directory itself (e.g. a domain associated to the
         tenant, such as contoso.onmicrosoft.com), and `<policy>` is the policy you wish to
         use for the current user flow. The policy we are using here is the `kSignupOrSigninPolicy`
         *as that is the policy we originally acquired the token with*. It is very important that all actions
         against a token acquired by a policy maintains the user of that policy on each call.
         */
        
        let kAuthority = String(format: kEndpoint, kTenantName, kSignupOrSigninPolicy)
        
        do {
            
            /**
             
             Initialize a MSALPublicClientApplication with a given clientID and authority
             
             - clientId:     The clientID of your application, you should get this from the app portal.
             - authority:    A URL indicating a directory that MSAL can use to obtain tokens. In Azure B2C
             it is of the form `https://<instance/tfp/<tenant>/<policy>`, where `<instance>` is the
             directory host (e.g. https://login.microsoftonline.com), `<tenant>` is a
             identifier within the directory itself (e.g. a domain associated to the
             tenant, such as contoso.onmicrosoft.com), and `<policy>` is the policy you wish to
             use for the current user flow.
             - error:       The error that occurred creating the application object, if any, if you're
             not interested in the specific error pass in nil.
             */
            
            let application = try MSALPublicClientApplication.init(clientId: kClientID, authority: kAuthority)
            
            /**
             Removes all tokens from the cache for this application for the provided user
             
             - user:    The user to remove from the cache
             */
            
            let thisUser = try self.getUserByPolicy(withUsers: application.users(), forPolicy: kSignupOrSigninPolicy)
            
            try application.remove(thisUser)

            
        } catch  {
            print("Received error signing user out: \(error)")
        }

    }
    
    func getUserByPolicy (withUsers: [MSALUser], forPolicy: String) throws -> MSALUser? {
        
        for user in withUsers {
            if (user.userIdentifier().components(separatedBy: ".")[0].hasSuffix(forPolicy.lowercased())) {
                return user
            }
        }
        return nil
    }

}
