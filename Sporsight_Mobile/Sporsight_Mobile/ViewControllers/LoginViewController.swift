//
//  LoginViewController.swift
//  Sporsight_m
//
//  Created by Christopher Donoso on 3/26/18.
//  Copyright © 2018 Microsoft. All rights reserved.
//

import AppCenter
import AppCenterAnalytics
import AppCenterCrashes
import Foundation
import LocalAuthentication
import MSAL
import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate, URLSessionDelegate{
    
    let kTenantName = "sporsightiam.onmicrosoft.com" // Your tenant name
    let kClientID = "7b66c778-b9c6-4e73-acf6-d26a969978be" // Your client ID from the portal when you created your application
    let kSigninPolicy = "b2c_1_login" // Your sign-in policy you created in the portal
    let kSignupPolicy = "b2c_1_register" // Your signup policy you created in the portal
    let kGraphURI = "https://sporsightiam.onmicrosoft.com/hello" // This is your backend API that you've configured to accept your app's tokens
    let kScopes: [String] = ["https://sporsightiam.onmicrosoft.com/hello/demo.read"] // This is a scope that you've configured your backend API to look for.
    
    //https://sporsightiam.onmicrosoft.com/hello/demo.read
    //https://sporsightiam.onmicrosoft.com/hello/user_impersonation
    
    // DO NOT CHANGE - This is the format of OIDC Token and Authorization endpoints for Azure AD B2C.
    let kEndpoint = "https://login.microsoftonline.com/tfp/%@/%@"
    
    var accessToken = String()
    var loggingText = String()

    override func viewDidLoad(){
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signUpButton(_ sender: Any) {
        
        let kAuthority = String(format: kEndpoint, kTenantName, kSignupPolicy)
        
        do {
            let application = try MSALPublicClientApplication.init(clientId: kClientID, authority: kAuthority)
            
            application.acquireToken(forScopes: kScopes) { (result, error) in
                if  error == nil {
                    self.accessToken = (result?.accessToken)!
                    print("Access token is \(self.accessToken)")
                    
                    let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    self.navigationController?.pushViewController(homeViewController, animated: true)
                    
                } else {
                    print("Could not acquire token: \(error ?? "No error informarion" as! Error)")
                }
            }
        } catch {
            print("Unable to create application \(error)")
        }
    }
    
    
    @IBAction func signInButton(_ sender: Any) {

        let kAuthority = String(format: kEndpoint, kTenantName, kSigninPolicy)
        
        do {
            let application = try MSALPublicClientApplication.init(clientId: kClientID, authority: kAuthority)
            
            application.acquireToken(forScopes: kScopes) { (result, error) in
                if  error == nil {
                    self.accessToken = (result?.accessToken)!
                    print("Access token is \(self.accessToken)")
                    
                    let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    self.navigationController?.pushViewController(homeViewController, animated: true)
                    
                } else {
                    print("Could not acquire token: \(error ?? "No error informarion" as! Error)")
                }
            }
        } catch {
            print("Unable to create application \(error)")
        }
    }
    
    //Dismisses keyboard.
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        self.view.endEditing(true)
    }
}
