//
//  PlayVideoViewController.swift
//  Sporsight_Mobile
//
//  Created by Jack Mustacato on 5/31/18.
//  Copyright © 2018 Microsoft. All rights reserved.
//

import AVFoundation
import MobileCoreServices
import ReplayKit
import SpriteKit
import UIKit

class PlayVideoViewController: UIViewController, UIImagePickerControllerDelegate {
    
    var player: AVPlayer?
    var playerLayer: AVPlayerLayer?
    var duration: CMTime?
    var seconds: Double?
    var playerObserver: Any?
    var holderView: SKView?
    var annotateView: AnnotateView?
    var toolBarItems: [UIView]!
    var recordingScreen: Bool = false
    var colorIndex: Int = 0
    var colors = [UIColor.white, UIColor.gray, UIColor.black, UIColor.red, UIColor.orange, UIColor.yellow, UIColor.green, UIColor.blue]
    
    var url: URL?
    
    @IBOutlet weak var toolbar: UIView!
    @IBOutlet weak var arrowButton: UIButton!
    @IBOutlet weak var circleButton: UIButton!
    @IBOutlet weak var rectangleButton: UIButton!
    @IBOutlet weak var freehandButton: UIButton!
    @IBOutlet weak var colorButton: UIButton!
    @IBOutlet weak var undoButton: UIButton!
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var finishButton: UIButton!
    
    @IBOutlet weak var controlsContainer: UIView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var currentDuration: UILabel!
    @IBOutlet weak var playhead: UISlider!
    @IBOutlet weak var totalDuration: UILabel!
    @IBOutlet weak var exitButton: UIButton!
    @IBOutlet weak var annotateButton: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        VideoHelper.startMediaBrowser(delegate: self, sourceType: .savedPhotosAlbum)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideControls))
        self.view.addGestureRecognizer(tap)
        
        self.view.backgroundColor = .black
        
        toolBarItems = [toolbar, arrowButton, circleButton, rectangleButton, freehandButton, recordButton, colorButton, undoButton, clearButton, finishButton]
        
        controlsContainer.layer.cornerRadius = 15
        controlsContainer.layer.masksToBounds = true
        controlsContainer.backgroundColor = .lightGray
        controlsContainer.alpha = 0.75
        
        exitButton.layer.cornerRadius = 5
        exitButton.layer.masksToBounds = true
        exitButton.backgroundColor = .lightGray
        exitButton.alpha = 0.75
        
        annotateButton.layer.cornerRadius = 5
        annotateButton.layer.masksToBounds = true
        annotateButton.backgroundColor = .lightGray
        annotateButton.alpha = 0.75
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if let observer = playerObserver {
            player?.removeTimeObserver(observer)
            playerObserver = nil
        }
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let mediaType = info[UIImagePickerControllerMediaType] as? String
            //mediaType == (kUTTypeMovie as String)
            url = info[UIImagePickerControllerMediaURL] as? URL
            //else { return }
        
        dismiss(animated: true) { [unowned self] in
            self.player = AVPlayer(url: self.url!)
            self.playhead.isContinuous = true
            self.duration = self.player?.currentItem?.asset.duration
            self.seconds = CMTimeGetSeconds(self.duration!)
            self.currentDuration.text = (self.seconds! > 3600) ? "00:00:00" : "00:00"
            self.totalDuration.text = self.stringFromTimeInterval(interval: self.seconds!)
            self.playhead.setValue(0, animated: true)
            
            let interval = CMTimeMakeWithSeconds(0.1, CMTimeScale(NSEC_PER_SEC))
            let mainQueue = DispatchQueue.main
            self.playerObserver = self.player?.addPeriodicTimeObserver(forInterval: interval, queue: mainQueue) {
                time in self.updatePlayhead(time)
            }
            
            self.playerLayer = AVPlayerLayer(player: self.player)
            self.playerLayer?.frame = self.view.frame
            
            self.view.layer.addSublayer(self.playerLayer!)
            
            self.enableControls()
        }
    }
    
    func enableControls() {
        view.bringSubview(toFront: controlsContainer)
        view.bringSubview(toFront: playButton)
        view.bringSubview(toFront: currentDuration)
        view.bringSubview(toFront: playhead)
        view.bringSubview(toFront: totalDuration)
        view.bringSubview(toFront: exitButton)
        view.bringSubview(toFront: annotateButton)
    }
    
    override func viewWillLayoutSubviews() {
        playerLayer?.frame = view.frame
        holderView?.frame = view.frame
    }
    
    @IBAction func exitButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func playButtonTapped(_ sender: UIButton) {
        if player?.timeControlStatus == .paused {
            player?.play()
            playButton.setImage(UIImage(named: "pauseButton"), for: .normal)
        } else {
            player?.pause()
            playButton.setImage(UIImage(named: "playButton"), for: .normal)
        }
    }
    
    func stringFromTimeInterval(interval: TimeInterval) -> String {
        let intInterval = Int(interval)
        let seconds = intInterval % 60
        let minutes = (intInterval / 60) % 60
        let hours = intInterval / 3600
        
        if hours > 0 {
            return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
        } else {
            return String(format: "%02d:%02d", minutes, seconds)
        }
    }
    
    @IBAction func playheadTouchBegin(_ sender: UISlider) {
        player?.pause()
        playButton.setImage(UIImage(named: "playButton"), for: .normal)
    }
    
    @IBAction func playheadValueChanged(_ sender: UISlider) {
        let duration = player?.currentItem?.asset.duration
        let newCurrentTime = CMTimeGetSeconds(duration!) * (Double)(sender.value)
        currentDuration.text = stringFromTimeInterval(interval: newCurrentTime)
        let seekTime = CMTimeMakeWithSeconds(newCurrentTime, 600)
        player?.seek(to: seekTime)
    }
    
    @IBAction func playheadTouchEnd(_ sender: UISlider) {
        let duration = player?.currentItem?.asset.duration
        let newCurrentTime = CMTimeGetSeconds(duration!) * (Double)(sender.value)
        let seekTime = CMTimeMakeWithSeconds(newCurrentTime, 600)
        player?.seek(to: seekTime)
        player?.play()
        playButton.setImage(UIImage(named: "pauseButton"), for: .normal)
    }
    
    func updatePlayhead(_ time: CMTime) {
        let duration = player?.currentItem?.asset.duration
        let current = player?.currentItem?.currentTime()
        let totalSeconds = CMTimeGetSeconds(duration!)
        let currentSeconds = CMTimeGetSeconds(current!)
        
        playhead.setValue(Float(currentSeconds / totalSeconds), animated: true)
        currentDuration.text = stringFromTimeInterval(interval: currentSeconds)
    }
    
    @IBAction func annotate(_ sender: UIButton) {
        if annotateView != nil {
            toggleToolBar()
            return
        }
        
        holderView = SKView()
        holderView?.backgroundColor = .clear
        
        guard holderView?.scene == nil else { return }
        
        annotateView = makeScene()
        holderView?.frame.size = (annotateView?.size)!
        view.addSubview(holderView!)
        holderView?.presentScene(annotateView!)
        annotateView?.annotationType = .freehand
        
        showToolBar()
        
        enableControls()
    }
    
    @objc func hideControls() {
        controlsContainer.isHidden = !controlsContainer.isHidden
        playButton.isHidden = !playButton.isHidden
        currentDuration.isHidden = !currentDuration.isHidden
        playhead.isHidden = !playhead.isHidden
        totalDuration.isHidden = !totalDuration.isHidden
        exitButton.isHidden = !exitButton.isHidden
        annotateButton.isHidden = !annotateButton.isHidden
    }
    
    func showToolBar() {
        toolbar.backgroundColor = .lightGray
        toolbar.alpha = 0.9
        toolbar.layer.cornerRadius = 10
        toolbar.layer.masksToBounds = true
        
        colorButton.layer.cornerRadius = 0.5 * colorButton.bounds.size.width
        colorButton.clipsToBounds = true
        colorIndex = 0
        colorButton.backgroundColor = colors[colorIndex]
        annotateView?.annotationColor = colors[colorIndex]
        
        arrowButton.titleLabel?.text = "A"
        circleButton.titleLabel?.text = "C"
        freehandButton.titleLabel?.text = "Fr"
        rectangleButton.titleLabel?.text = "Rt"
        recordButton.titleLabel?.text = "Rc"
        undoButton.titleLabel?.text = "U"
        finishButton.titleLabel?.text = "Fn"
        
        for item in toolBarItems {
            item.isHidden = !item.isHidden
            view.bringSubview(toFront: item)
        }
    }
    
    func toggleToolBar() {
        for item in toolBarItems {
            item.isHidden = !item.isHidden
        }
    }
    
    @IBAction func setArrow(_ sender: Any) {
        annotateView?.annotationType = .arrow
    }
    
    @IBAction func setRectangle(_ sender: Any) {
        annotateView?.annotationType = .rectangle
    }
    
    @IBAction func setFreehand(_ sender: Any) {
        annotateView?.annotationType = .freehand
    }
    
    @IBAction func setCircle(_ sender: Any) {
        annotateView?.annotationType = .circle
    }
    
    @IBAction func undoAnnotation(_ sender: Any) {
        guard annotateView?.paths.isEmpty == false else { return }
        annotateView?.paths.last?.removeFromParent()
        annotateView?.paths.removeLast()
    }
    
    @IBAction func clearAnnotations(_ sender: Any) {
        guard annotateView?.paths.isEmpty == false else { return }
        annotateView?.removeAllChildren()
        annotateView?.paths.removeAll(keepingCapacity: true)
    }
    
    @IBAction func shareButton(_ sender: Any) {
    
        let ac = UIActivityViewController(activityItems: [url as Any], applicationActivities: nil)
        present(ac, animated: true)
    }
    
    
    @IBAction func finishAnnotating(_ sender: Any) {
        holderView?.presentScene(nil)
        annotateView = nil
        holderView?.removeFromSuperview()
        toggleToolBar()
        
        if recordingScreen {
            let recorder = RPScreenRecorder.shared()
            
            recorder.stopRecording { [unowned self] (preview, error) in
                if error != nil {
                    print(error!.localizedDescription)
                    self.recordingScreen = false
                } else {
                    if let unwrappedPreview = preview {
                        self.recordingScreen = false
                        unwrappedPreview.previewControllerDelegate = self
                        self.present(unwrappedPreview, animated: true)
                    }
                }
            }
        }
    }
    
    @IBAction func screenRecord(_ sender: Any) {
        let recorder = RPScreenRecorder.shared()
        
        if recordingScreen == false {
            recorder.startRecording { [unowned self] (error) in
                if error != nil {
                    print(error!.localizedDescription)
                } else {
                    self.recordingScreen = true
                }
            }
        } else {
            recorder.stopRecording { [unowned self] (preview, error) in
                if error != nil {
                    print(error!.localizedDescription)
                    self.recordingScreen = false
                } else {
                    if let unwrappedPreview = preview {
                        self.recordingScreen = false
                        unwrappedPreview.previewControllerDelegate = self
                        self.present(unwrappedPreview, animated: true)
                    }
                }
            }
        }
    }
    
    @IBAction func changeAnnotationColor(_ sender: Any) {
        if colorIndex + 1 >= colors.count {
            colorIndex = 0
        } else {
            colorIndex += 1
        }
        
        colorButton.backgroundColor = colors[colorIndex]
        annotateView?.annotationColor = colors[colorIndex]
    }
}

extension PlayVideoViewController {
    func makeScene() -> AnnotateView {
        let minimumDimension = min(view.frame.width, view.frame.height)
        let size = CGSize(width: minimumDimension, height: minimumDimension)
        
        let scene = AnnotateView(size: size)
        scene.backgroundColor = .clear
        return scene
    }
}

// MARK: - RPPreviewViewControllerDelegate

extension PlayVideoViewController: RPPreviewViewControllerDelegate {
    func previewControllerDidFinish(_ previewController: RPPreviewViewController) {
        dismiss(animated: true)
    }
}

// MARK: - UINavigationControllerDelegate

extension PlayVideoViewController: UINavigationControllerDelegate {
}
