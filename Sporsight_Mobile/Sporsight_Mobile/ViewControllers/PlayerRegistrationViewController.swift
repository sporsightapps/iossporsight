//
//  PlayerRegistrationViewController.swift
//  Sporsight_m
//
//  Created by Christopher Donoso on 4/10/18.
//  Copyright © 2018 Microsoft. All rights reserved.
//

import Foundation
import UIKit

class PlayerRegistrationViewController: UIViewController, UITextFieldDelegate{
    
    @IBOutlet weak var firstNameTextField: YoshikoTextField!
    @IBOutlet weak var lastNameTextField: YoshikoTextField!
    @IBOutlet weak var emailTextField: YoshikoTextField!
    @IBOutlet weak var roleTextField: YoshikoTextField!
    @IBOutlet weak var schoolNameTextField: YoshikoTextField!
    @IBOutlet weak var uniqueIDTextField: UITextField!

    var uniqueIDText:String = ""

    override func viewDidLoad(){
        super.viewDidLoad()
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SchoolRegistrationViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard(){
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    //Checks to see if the email is valid.
    func isValidEmail(testStr:String) -> Bool{
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        
        return result
    }
    
    //Submits the player's information to the database.
    @IBAction func signUpButton(_ sender: Any){
        
        //Checks if any of the registration fields are empty.
        if (firstNameTextField.text?.isEmpty)! || (lastNameTextField.text?.isEmpty)! || (emailTextField.text?.isEmpty)! || (roleTextField.text?.isEmpty)! || (schoolNameTextField.text?.isEmpty)!{
            let alert = UIAlertController(title: "Error", message: "Please fill all fields.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                NSLog("The \"OK\" alert occured.")
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
        //Check if the user provided a valid email.
        if !(isValidEmail(testStr: emailTextField.text!)){
            let alert = UIAlertController(title: "Error", message: "Please enter a valid email.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                NSLog("The \"OK\" alert occured.")
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
        //Information is sent to the database.
        else{
            let delegate = UIApplication.shared.delegate as! AppDelegate
            let client = delegate.client!
            
            let item = ["clientFirst":String(firstNameTextField.text!), "clientLast":String(lastNameTextField.text!), "clientEmail":String(emailTextField.text!), "clientRole":String(roleTextField.text!), "schoolName":String(schoolNameTextField.text!), "clientID":String(uniqueIDText)]
            
            let itemTable = client.table(withName: "Client")
            
            itemTable.insert(item) { (result, error) in
                if let err = error {
                    print("ERROR ", err)
                    let alert = UIAlertController(title: "Error", message: "Unable to register.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                        NSLog("The \"OK\" alert occured.")
                    }))
                    self.present(alert, animated: true, completion: nil)
                } else if result != nil {
                    
//                    let alert = UIAlertController(title: "Success", message: "Successfully registered.", preferredStyle: .alert)
//                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
//                        NSLog("The \"OK\" alert occured.")
//                    }))
//                    self.present(alert, animated: true, completion: nil)
                    
                    let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    self.navigationController?.pushViewController(homeViewController, animated: true)
                    
                }
            }
        }
    }
}
