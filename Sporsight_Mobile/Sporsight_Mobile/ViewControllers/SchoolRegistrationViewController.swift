//
//  SchoolRegistrationViewController.swift
//  Sporsight_Mobile
//
//  Created by Christopher Donoso on 5/5/18.
//  Copyright © 2018 Microsoft. All rights reserved.
//

import Foundation

import Foundation
import UIKit

class SchoolRegistrationViewController: UIViewController, UITextFieldDelegate{
        
    @IBOutlet weak var schoolNameTextField: YoshikoTextField!
    @IBOutlet weak var schoolAddressTextField: YoshikoTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SchoolRegistrationViewController.dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)
    }
    
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    //Submits the school's information to the database.
    @IBAction func signUpButton(_ sender: Any){
        //Throws a pop up error if any of the registration fields are empty.
        if (schoolNameTextField.text?.isEmpty)! || (schoolAddressTextField.text?.isEmpty)!{
            let alert = UIAlertController(title: "Error", message: "Please fill all fields.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                NSLog("The \"OK\" alert occured.")
            }))
            self.present(alert, animated: true, completion: nil)
        }
            
        else{
            let delegate = UIApplication.shared.delegate as! AppDelegate
            let client = delegate.client!
            let item = ["schoolName":String(schoolNameTextField.text!), "schoolLocation":String(schoolAddressTextField.text!)]
            
            let itemTable = client.table(withName: "School")
            itemTable.insert(item)
            {
                (insertedItem, error) in
                if (error != nil) {
                    print("Error" + error.debugDescription);
                    let alert = UIAlertController(title: "Error", message: "Unable to register.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                        NSLog("The \"OK\" alert occured.")
                    }))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    print("Items inserted")
                    let alert = UIAlertController(title: "Success", message: "Successfully registered.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                        NSLog("The \"OK\" alert occured.")
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
}
