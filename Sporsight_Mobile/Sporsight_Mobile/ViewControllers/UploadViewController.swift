//
//  UploadViewController.swift
//  Sporsight_Mobile
//
//  Created by Jack Mustacato on 8/29/18.
//  Copyright © 2018 Microsoft. All rights reserved.
//

import MobileCoreServices
import UIKit

class UploadViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    let containerName = "containerpublic"
    var blobName = "09-19-2018_14:27:20"
    let connectionString = "DefaultEndpointsProtocol=https;AccountName=sporsightmobile;AccountKey=/Je3FmjgOQ8rswYUAEMkXf1mIwB9twwyw5cZZkHLijiiSwSxwvVfftM6z/X1SX9KuS3O6mtzW6i3Gfhr2p9mNA==;EndpointSuffix=core.windows.net"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func getDocumentsDirectory() -> URL {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    }
    
    func setupContainer(with name: String) throws -> AZSCloudBlobContainer {
        let account = try AZSCloudStorageAccount(fromConnectionString: connectionString)
        let blobClient = account.getBlobClient()
        let blobContainer = blobClient.containerReference(fromName: name)
        
        return blobContainer
    }
    
    @IBAction func createContainer(_ sender: UIButton) {
        do {
            let blobContainer = try setupContainer(with: containerName)
            
            let condition = NSCondition()
            var containerCreated = false
            
            blobContainer.createContainerIfNotExists(with: AZSContainerPublicAccessType.container, requestOptions: nil, operationContext: nil) { (error, exists) in
                if let error = error {
                    print("Error creating container: \(error.localizedDescription)")
                }
                condition.lock()
                containerCreated = true
                condition.signal()
                condition.unlock()
            }
            
            condition.lock()
            while (!containerCreated) {
                condition.wait()
            }
            condition.unlock()
        } catch {
            print("Error connecting to account: \(connectionString)")
        }
    }
    
    @IBAction func listBlobsInContainer(_ sender: UIButton) {
        do {
            let blobContainer = try setupContainer(with: containerName)
            
            listBlobsInContainerHelper(container: blobContainer, continuationToken: nil, prefix: nil, listingDetails: AZSBlobListingDetails.all, maxResults: -1) { (error) in
                if let error = error {
                    print("Error listing blobs: \(error.localizedDescription)")
                }
            }
        } catch {
            print("Error connecting to account: \(connectionString)")
        }
    }
    
    func listBlobsInContainerHelper(container: AZSCloudBlobContainer, continuationToken: AZSContinuationToken?, prefix: String?, listingDetails: AZSBlobListingDetails, maxResults: Int, completionHandler: @escaping (Error?) -> (Void)) {
        
        container.listBlobsSegmented(with: continuationToken, prefix: prefix, useFlatBlobListing: true, blobListingDetails: listingDetails, maxResults: maxResults) { [unowned self] (error, results) in
            if let error = error {
                completionHandler(error)
            } else {
                if let results = results {
                    if let blobs = results.blobs as? [AZSCloudBlockBlob] {
                        for blob in blobs {
                            print(blob.blobName)
                        }
                        
                        if results.continuationToken != nil {
                            self.listBlobsInContainerHelper(container: container, continuationToken: results.continuationToken, prefix: prefix, listingDetails: listingDetails, maxResults: maxResults, completionHandler: completionHandler)
                        } else {
                            completionHandler(nil)
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func uploadVideo(_ sender: UIButton) {
        VideoHelper.startMediaBrowser(delegate: self, sourceType: .savedPhotosAlbum)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let mediaType = info[UIImagePickerControllerMediaType] as? String,
            mediaType == (kUTTypeMovie as String),
            let url = info[UIImagePickerControllerMediaURL] as? URL
            else { return }
        
        dismiss(animated: true) { [unowned self] in
            do{
                let blobContainer = try self.setupContainer(with: self.containerName)
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM-dd-yyyy_HH:mm:ss"
                self.blobName = dateFormatter.string(from: Date())
                
                let blockBlob = blobContainer.blockBlobReference(fromName: self.blobName)
                blockBlob.uploadFromFile(with: url) { [unowned self] (error) in
                    if let error = error {
                        print("Error uploading blob: \(error.localizedDescription)")
                    } else {
                        let ac = UIAlertController(title: "Video uploaded successfully", message: nil, preferredStyle: .alert)
                        ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self.present(ac, animated: true)
                    }
                }
            } catch {
                print("Error connecting to account: \(self.connectionString)")
            }
        }
    }
    
    @IBAction func downloadBlob(_ sender: UIButton) {
        do {
            let blobContainer = try setupContainer(with: containerName)
            let blockBlob = blobContainer.blockBlobReference(fromName: blobName)
            
            let url = getDocumentsDirectory().appendingPathComponent("videoBlob").appendingPathExtension("mp4")
            
            blockBlob.downloadToFile(with: url, append: false) { [unowned self] (error) in
                if let error = error {
                    print("Error downloading blob: \(error.localizedDescription)")
                }
                if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(url.path) {
                    UISaveVideoAtPathToSavedPhotosAlbum(url.path, self, nil, nil)
                    let ac = UIAlertController(title: "Video downloaded to photos album", message: nil, preferredStyle: .alert)
                    ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(ac, animated: true)
                } else {
                    print("Error saving video")
                }
            }
        } catch {
            print("Error connecting to account: \(connectionString)")
        }
    }
    
}
